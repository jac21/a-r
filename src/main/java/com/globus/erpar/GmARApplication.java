package com.globus.erpar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication(scanBasePackages = { "com.globus.common", "com.globus.erpar" })
@EntityScan(basePackages = { "com.globus.common.model", "com.globus.erpar.model" })
public class GmARApplication extends SpringBootServletInitializer {
	
	/**
	 * Initalize the Spring Boot application
	 * 
	 */
	public static void main(String[] args) {
		SpringApplication.run(GmARApplication.class, args);
	}

}
