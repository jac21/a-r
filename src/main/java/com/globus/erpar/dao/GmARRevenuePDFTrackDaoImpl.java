package com.globus.erpar.dao;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.ParameterMode;
import com.globus.common.dao.GmEntityManager;
import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARRevenuePDFTrackModel;
import com.globus.erpar.rowmapper.GmARRevenuePDFTrackRowMapper;

/**
 * This GmARRevenuePDFTrackDaoImpl class used to fetch/save auto revenue tracking details
 * DAO implementation 
 */
@Transactional
@Repository
@Component
public class GmARRevenuePDFTrackDaoImpl implements GmARRevenuePDFTrackDao{
	
	@Autowired
	GmCommonClass gmCommonClass;
	
	@Autowired
	GmEntityManager gmEntityManager;
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public GmARRevenuePDFTrackDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	/**
	 * This method is used to fetch revenue count by aging and category
	 * @param GmARRevenuePDFTrackModel
	 * @return int
	 */
	public int fetchRevenueTrackCnt(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception{
		String strCompanyId = gmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompanyId());
		String strDateFmt = gmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrDtFormat());
		String strCompZone = gmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompZone());
		String strAging = gmCommonClass.parseZero(gmARRevenuePDFTrackModel.getStrAging());
		
		StringBuilder strQuery = new StringBuilder();
			strQuery.append(" SELECT count(1) FROM t5005_ar_revenue_scan_dtls t5005,t501_order t501 ");
			strQuery.append(" WHERE t5005.c501_order_id = t501.c501_order_id AND t5005.c5005_approve_fl is null ");
			strQuery.append(" AND t5005.c5005_void_fl is null AND t501.c501_void_fl is null ");
			strQuery.append(" AND t5005.c1900_company_id="+gmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompanyId()));
			strQuery.append(" AND t5005.c901_category="+gmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCategory()));		
			strQuery.append(getAgingQuery(strAging,strDateFmt,strCompZone));
			System.out.println("Revenuecnt - query == "+strQuery.toString());
			
			Query cntQuery = em.createNativeQuery(strQuery.toString());
			 String strCnt = ((Number) cntQuery.getSingleResult()).toString();
			return Integer.parseInt(strCnt);
	}
	
	/**
	 * This method is used to fetch revenue report aging query string
	 * @param strAging,strDateFmt,strCompZone
	 * @return String
	 */	
	private String getAgingQuery(String strAging,String strDateFmt,String strCompZone) throws Exception{
		String strQuery="";
		ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of(strCompZone));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(strDateFmt);  
		
		if(Integer.parseInt(gmCommonClass.parseZero(strAging))>2) {
			strQuery = " AND TRUNC(t5005.c5005_load_date) < TRUNC(to_date('"+formatter.format(zonedDateTime.toLocalDate())+"','"+strDateFmt+"')-2) ";
		}else {
			strQuery = " AND TRUNC(t5005.c5005_load_date) = TRUNC(to_date('"+formatter.format(zonedDateTime.toLocalDate())+"','"+strDateFmt+"')-"+strAging+") ";
		}
		return strQuery;
	}

	/**
	 * This method is used to fetch revenue report by filters 
	 * @param GmARRevenuePDFTrackModel
	 * @return GmARRevenueTrackModel List
	 */	
	@Override
	public List<GmARRevenuePDFTrackModel> fetchRevenueTrackRpt(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		String strDateFmt = gmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrDtFormat());
		String strCompZone = gmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompZone());
		ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of(strCompZone));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(strDateFmt); 
		String strDOId = gmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrOrderId());
		String strAging = gmCommonClass.parseZero(gmARRevenuePDFTrackModel.getStrAging());
		
		if(!strDOId.equals("")){
			strDOId = strDOId.replaceAll(",", "','");
		}
		
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("select C5005_AR_REVENUE_SCAN_DTLS_ID revenueTrackId,t501.c501_order_id strOrderId,");
		strQuery.append("DECODE ('', '103097', NVL (T704.C704_ACCOUNT_NM_EN, T704.C704_ACCOUNT_NM),T704.C704_ACCOUNT_NM) strRepAcc,");
		strQuery.append("TO_CHAR (t501.c501_order_date,'"+strDateFmt+"') strOrderdate,");
		strQuery.append("t901_state.c901_code_nm strState");
		strQuery.append(",t901_category.c901_code_nm strCategory");
		strQuery.append(",DECODE(NVL(t704a.c704a_attribute_value,9999),903100,'Y','') strContractFlag,");
		strQuery.append(" gm_pkg_ar_auto_revenue_track.get_reveue_track_aging(trunc(t5005.c5005_load_date),to_date('"+formatter.format(zonedDateTime.toLocalDate())+"','"+strDateFmt+"')) stragingnm,");
		strQuery.append(" t901_po_rev.c901_code_nm strPoRevenue, ");
		strQuery.append(" t901_do_rev.c901_code_nm strDoRevenue, ");
		strQuery.append(" t901_override_do.c901_code_nm strOverideDo, ");
		strQuery.append(" t901_override_po.c901_code_nm strOveridePo, ");
		strQuery.append(" t5005.C5005_COMMENTS strComments ");
		strQuery.append(" from t5005_ar_revenue_scan_dtls t5005,t501_order t501,t704_account t704,");
		strQuery.append("t703_sales_rep t703,t701_distributor t701,t704a_account_attribute t704a,");
		strQuery.append("t901_code_lookup t901_state,t901_code_lookup t901_category,");
		strQuery.append("	t901_code_lookup t901_po_rev,t901_code_lookup t901_do_rev,t901_code_lookup t901_override_do,t901_code_lookup t901_override_po ");
		strQuery.append(" where t5005.c501_order_id = t501.c501_order_id");
		strQuery.append(" and t5005.c5005_approve_fl is null");
		strQuery.append(" and t704.c704_account_id = t501.c704_account_id");
		strQuery.append(" and t703.c703_sales_rep_id = t501.c703_sales_rep_id");
		strQuery.append(" and t703.c701_distributor_id = t701.c701_distributor_id");
		strQuery.append(" and t704.c704_account_id = t704a.c704_account_id(+)");
		strQuery.append(" and t704a.c901_attribute_type(+) = 903103");
		strQuery.append(" and t704a.c704a_void_fl(+) is null");
		strQuery.append(" and t5005.c5005_void_fl is null");
		strQuery.append(" and t501.c501_void_fl is null");
		strQuery.append(" and t704.c704_void_fl is null");
		strQuery.append(" and t703.c703_void_fl is null");
		strQuery.append(" and t701.c701_void_fl is null");
		strQuery.append(" and t901_state.c901_code_id(+) = t704.c704_ship_state");
		strQuery.append(" and t901_category.c901_code_id = t5005.c901_category");
		strQuery.append(" and t901_po_rev.c901_code_id(+) =  t5005.c901_system_recog_po_val");
		strQuery.append(" and t901_do_rev.c901_code_id(+) =  t5005.c901_system_recog_do_val");
		strQuery.append(" and t901_override_do.c901_code_id(+) = t5005.c901_override_do_val");
		strQuery.append(" and t901_override_po.c901_code_id(+) = t5005.c901_override_po_val");
		
		
		if(!GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrAccid()).equals("")){
			strQuery.append(" AND T704.c704_account_id = '"+ GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrAccid()) +"' ");
		}
		
		if(!GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrRepAcc()).equals("")){
			strQuery.append(" AND T704.c704_account_id = '"+ GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrRepAcc()) +"' ");
		}
		
		if(!GmCommonClass.parseZero(gmARRevenuePDFTrackModel.getStrSalesRep()).equals("0")){
			strQuery.append(" AND T703.C703_SALES_REP_ID = '"+ GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrSalesRep()) +"' ");
		}
		
		if(!GmCommonClass.parseZero(gmARRevenuePDFTrackModel.getStrFieldSales()).equals("0")){
			strQuery.append(" AND T701.C701_DISTRIBUTOR_ID = '"+ GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrFieldSales()) +"' ");
		}
		
		if(!GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrOrdStartDate()).equals("")){
			strQuery.append(" AND T501.C501_ORDER_DATE >= to_date('"+GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrOrdStartDate())+"','"+GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrDtFormat())+"') ");
		}
		
		if(!GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrOrdEndDate()).equals("")){
			strQuery.append(" AND T501.C501_ORDER_DATE <= to_date('"+GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrOrdEndDate())+"','"+GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrDtFormat())+"') ");
		}
		
		if(!GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrOrderId()).equals("")){
			strQuery.append(" AND T501.C501_ORDER_ID in ('"+ strDOId +"') ");
		}
		
		if(!GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCategory()).equals("")){
			strQuery.append(" AND t5005.c901_category = "+ GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCategory()) +" ");
		}
		
		if(!GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrState()).equals("")){
			strQuery.append(" AND t704.C704_SHIP_STATE = '"+ GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrState()) +"' ");
		}
		
		if(!GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrAging()).equals("")){
			strQuery.append(getAgingQuery(strAging,strDateFmt,strCompZone));
		}
		
		strQuery.append(" and t5005.c1900_company_id="+GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompanyId()));
		
		strQuery.append(" order by UPPER(t704.C704_ACCOUNT_NM),t501.c501_order_date,t901_category.c901_code_nm ");
		System.out.println("fetchDORevenueRpt - query == "+strQuery.toString());
		List<GmARRevenuePDFTrackModel> getRevenueModel = this.jdbcTemplate.query(strQuery.toString(),new GmARRevenuePDFTrackRowMapper());
		return getRevenueModel;
	}
	
	/**
	 * This method is used to save override PO revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@Override
	public void saveOverridePORevenue(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		String strCompanyId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompanyId());
		String strUserId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrUserId());
		String strPlantId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrPlantId());
		String strCompZone = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompZone());
		gmEntityManager.getConnection(strCompanyId, strPlantId, strCompZone);
		
		StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_pkg_ar_auto_revenue_track.gm_sav_override_po_revenue")
				.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
	    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
	    		.registerStoredProcedureParameter(3,String.class,ParameterMode.IN)
	    		.setParameter(1, GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrOrderId()))
		        .setParameter(2, GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrPoRevenue()))
		        .setParameter(3, GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrUserId()));
		        storedProcedureQuery.execute();
	}

	/**
	 * This method is used to save override DO revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@Override
	public void saveOverrideDORevenue(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		String strCompanyId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompanyId());
		String strUserId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrUserId());
		String strPlantId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrPlantId());
		String strCompZone = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompZone());
		gmEntityManager.getConnection(strCompanyId, strPlantId, strCompZone);
		
		StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_pkg_ar_auto_revenue_track.gm_sav_override_do_revenue")
				.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
	    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
	    		.registerStoredProcedureParameter(3,String.class,ParameterMode.IN)
	    		.setParameter(1, GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrOrderId()))
		        .setParameter(2, GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrDoRevenue()))
		        .setParameter(3, GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrUserId()));
		        storedProcedureQuery.execute();
	}
	
	/**
	 * This method is used to save comments revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@Override
	public void saveDORevenueComment(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		String strCompanyId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompanyId());
		String strUserId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrUserId());
		String strPlantId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrPlantId());
		String strCompZone = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompZone());
		gmEntityManager.getConnection(strCompanyId, strPlantId, strCompZone);
		
		StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_pkg_ar_auto_revenue_track.gm_sav_revenue_comments")
				.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
	    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
	    		.registerStoredProcedureParameter(3,String.class,ParameterMode.IN)
	    		.setParameter(1,  GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrOrderId()))
		        .setParameter(2,  GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrComments()))
		        .setParameter(3,  GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrUserId()));
		        storedProcedureQuery.execute();
	}
	
	
	/**
	 * This method is used to approve revenue track  details
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@Override
	public void approveRevenueTrack(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		String strCompanyId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompanyId());
		String strUserId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrUserId());
		String strPlantId = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrPlantId());
		String strCompZone = GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrCompZone());
		gmEntityManager.getConnection(strCompanyId, strPlantId, strCompZone);
		
		StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_pkg_ar_auto_revenue_track.gm_sav_revenue_approve")
				.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
	    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
	    		.setParameter(1, GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrApproveIds()))
		        .setParameter(2, GmCommonClass.parseNull(gmARRevenuePDFTrackModel.getStrUserId()));
		        storedProcedureQuery.execute();
	}

}
