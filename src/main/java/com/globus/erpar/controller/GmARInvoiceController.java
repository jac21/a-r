package com.globus.erpar.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.globus.common.model.GmUser;
import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmARInvoiceCommonModel;
import com.globus.erpar.model.GmARInvoiceModel;
import com.globus.erpar.model.GmAROrderRevenueModel;
import com.globus.erpar.service.GmARInvoiceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

@RequestMapping("/api")
@RestController
public class GmARInvoiceController {

	@Autowired
	GmARInvoiceService gmARInvoiceService;

	/**
	 * This method is used to create the batch
	 * 
	 * @param String
	 * @return String
	 */
	@RequestMapping(value = "saveBatchProcess", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String saveBatchProcess(@RequestBody GmARInvoiceModel gmARInvoiceModel) throws Exception {
		return gmARInvoiceService.saveBatchProcess(gmARInvoiceModel);
	}

	/**
	 * This method is used to fetching the orders by PODtlsId for Associated Orders popup
	 */	
	@RequestMapping(value = "fetchOrdersByPO", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARCustPOModel> fetchOrdersByPO(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		return gmARInvoiceService.fetchOrdersByPO(gmARCustPOModel);
	}

	/**
	 * This method is used to fetch PODetails by PODtsId for Associated Orders popup
	 */
	@RequestMapping(value = "fetchPODetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARCustPOModel> fetchPODetails(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		return gmARInvoiceService.fetchPODetails(gmARCustPOModel);
	}

}
