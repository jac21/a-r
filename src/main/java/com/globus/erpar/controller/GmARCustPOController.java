package com.globus.erpar.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.globus.common.model.GmUser;
import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmAROrderRevenueModel;
import com.globus.erpar.model.GmARRejectPOEmailModel;
import com.globus.erpar.service.GmARCustPOService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;


/**
 * This GmARCustPOController class used to load/save the Order Revenue Details,Order PO Details and PO Status. 
 * 
 */

@RequestMapping("/api")
@RestController
public class GmARCustPOController {

	@Autowired
	GmARCustPOService gmARCustPOService;
	
	/**
	 * This method is used to load the PO report details
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	
	@RequestMapping(value = "loadPOreport", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARCustPOModel> loadPendingPO(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		return gmARCustPOService.fetchPendingPODetails(gmARCustPOModel);
	}
	/**
	 * This method is used to fetch the Order and PO Details for selected Order
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	
	@RequestMapping(value = "loadOrderDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARCustPOModel> fetchOrderDetails(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		return gmARCustPOService.fetchOrderDetails(gmARCustPOModel);
	}	
	/**
	 * This method is used to fetch the Order Revenue Details for selected Order
	 * @param GmAROrderRevenueModel
	 * @return GmAROrderRevenueModel
	 */
	
	@RequestMapping(value = "loadOrderRevenueDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public GmAROrderRevenueModel fetchOrderRevenueDetails(@RequestBody GmAROrderRevenueModel gmAROrderRevenueModel) throws Exception {
		return gmARCustPOService.fetchOrderRevenueDetails(gmAROrderRevenueModel);
	}
    /**
	 * This method used to save/update Order Revenue Details for selected Order
	 * @param - GmARCustPOModel
	 * @return - GmAROrderRevenueModel
	 */	
	@RequestMapping(value = "saveRevenueDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<GmAROrderRevenueModel> saveOrderRevenueDetails(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		return gmARCustPOService.saveOrderRevenueDetails(gmARCustPOModel);
	}	
	/**
	 * This method is used to save/update Order PO Details
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */
	@RequestMapping(value = "savePODetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARCustPOModel>  saveOrderPODetails(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		
		return gmARCustPOService.saveOrderPODetails(gmARCustPOModel);
	}
	/**
	 * This method used to validate the current PO with Existing PO
	 * @param - strPONumber,strCompanyId
	 * @return - Count as String
	 */
	@RequestMapping(value = "validatePONum/{strPONumber}/{strCompanyId}/{strOrderId}", method = RequestMethod.GET)
	public String validatePO(@PathVariable String strPONumber,@PathVariable String strCompanyId,@PathVariable String strOrderId)
			throws Exception {
		
		return gmARCustPOService.validatePO(strPONumber,strCompanyId,strOrderId);
	}
	/**
	 * This method is used to fetch the Existing PO Details for entered PO Number
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	@RequestMapping(value = "fetchPODrilldownDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARCustPOModel> fetchPODrilldown(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		return gmARCustPOService.fetchPODrilldown(gmARCustPOModel);
	}
	
	/**
	 * This method is used to save/update PO Status(Ready For Invoicing,Move to Discrepancy,Reject)
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	
	@RequestMapping(value = "savePOStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveOrderPOStatus(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		 gmARCustPOService.saveOrderPOStatus(gmARCustPOModel);
	}
	/**
	   * Description: This method is used to get report PO status Report
	   * Author
	   * @param GmARCustPOModel
	   * @return List<GmARCustPOModel>
	   * @exception Exception 
	   **/
	@RequestMapping(value = "loadPOByStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARCustPOModel> fetchPOByStatus(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception{
		return gmARCustPOService.fetchReadyForInvoiceDtls(gmARCustPOModel);
	}
	
	/*
	 * This method used to save/update PO's discrepancy Details for selected PO
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */	
	@RequestMapping(value = "saveDiscrepancyDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveDiscrepancyDetails(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		gmARCustPOService.saveDiscrepancyDetails(gmARCustPOModel);
	}
	/*
	 * This method used to fetch PO Format drill down details based on account
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */	
	@RequestMapping(value = "fetchPOFormatDrilldownDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARCustPOModel> fetchPOFormatDrilldownDetails(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		return gmARCustPOService.fetchPOFormatDrilldownDetails(gmARCustPOModel);
	}
		/*
	 * This method used to fetch Reject PO Notify Email details
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */	
	@RequestMapping(value = "fetchRejectPONotifyEmailDtls", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARRejectPOEmailModel> fetchRejectPONotifyEmailDtls(@RequestBody GmARRejectPOEmailModel gmARRejectPOEmailModel) throws Exception {
		return gmARCustPOService.fetchRejectPONotifyEmailDtls(gmARRejectPOEmailModel);
	}
	/*
	 * This method used to move PO's into Processor Error for selected PO
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */	
	@RequestMapping(value = "saveProcessorErrorDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveProcessorErrorDetails(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		gmARCustPOService.saveProcessorErrorDetails(gmARCustPOModel);
	}
}
