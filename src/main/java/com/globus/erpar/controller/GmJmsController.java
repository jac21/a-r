package com.globus.erpar.controller;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import com.globus.common.jms.GmJMSUtils;
import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARInvoiceCommonModel;

import java.util.HashMap;
/**
 * This GmJmsController class used to call the JMS to spine it
 * 
 */
@RequestMapping("/api")
@RestController
public class GmJmsController {
	/**
	   * Description: arbatchjmscall - this method is used to call the jms
	   * Author
	   * @param GmARInvoiceCommonModel
	   * @exception Exception 
	   **/
	@Autowired
	GmCommonClass gmCommonClass;
	@RequestMapping(value = "arbatchjmscall",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void jmsCall(@RequestBody GmARInvoiceCommonModel gmARInvoiceCommonModel) throws Exception {
		
		String strMessage = gmARInvoiceCommonModel.getStrMessage();
		String strUserId = gmARInvoiceCommonModel.getStrUserId();
		String strLangId = gmARInvoiceCommonModel.getStrLangId();
		String strCompanyId = gmARInvoiceCommonModel.getStrCompanyId();
		String strPartyId = gmARInvoiceCommonModel.getStrPartyId();
		String strPlantId = gmARInvoiceCommonModel.getStrPlantId();
		
		String strCompanyInfo = "{\"cmpid\":\""+ strCompanyId+ "\",\"plantid\":\""+ strPlantId +"\",\"token\":\"\",\"partyid\":\""+ strPartyId + "\",\"cmplangid\":\""+ strLangId + "\"}";
		// to set the AR batch details

		String strQueueName = gmCommonClass.getProperty("queueName");
		String strConsumerClass = gmCommonClass.getProperty("arconsumerClass");
		
		HashMap hmMessageObj = new HashMap ();
		hmMessageObj.put("USERID", strUserId);
		hmMessageObj.put("BATCHID", strMessage);
        hmMessageObj.put("COMPANY_ID", strCompanyId);
	    hmMessageObj.put("companyInfo", strCompanyInfo);
	    
        // to set the JMS class name and queue name
        hmMessageObj.put("CONSUMERCLASS", strConsumerClass );
        hmMessageObj.put("QUEUE_NAME", strQueueName );
    	System.out.println("hmMessageObj------->"+hmMessageObj);
		// actual code
		GmJMSUtils gmJMSUtils = new GmJMSUtils();
		gmJMSUtils.sendMessage(hmMessageObj, strQueueName, strConsumerClass);
	}
	
}

