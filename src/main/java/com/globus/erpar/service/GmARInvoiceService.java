package com.globus.erpar.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.PathVariable;

import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmARInvoiceModel;

public interface GmARInvoiceService {
	/**
	 * This method is used to create the batch.
	 * @param String
	 * @return String
	 */
	public String saveBatchProcess(GmARInvoiceModel gmARInvoiceModel) throws Exception;
	/**
	 * This method is used to fetching the orders by PODtlsId for Associated Orders popup
	 */	
	public List<GmARCustPOModel> fetchOrdersByPO(GmARCustPOModel gmARCustPOModel) throws Exception;
	/**
	 * This method is used to fetch PODetails by PODtsId for Associated Orders popup
	 */
	public List<GmARCustPOModel> fetchPODetails(GmARCustPOModel gmARCustPOModel) throws Exception;
	
}
