package com.globus.erpar.service;

import com.globus.erpar.dao.GmARRevenuePDFTrackDao;
import com.globus.erpar.model.GmARRevenuePDFTrackModel;

import org.springframework.stereotype.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * This GmARRevenuePDFTrackServiceImpl class used to fetch/save auto revenue tracking details
 * Service implementation 
 */
@Service
public class GmARRevenuePDFTrackServiceImpl implements GmARRevenuePDFTrackService{
	@Autowired
	GmARRevenuePDFTrackDao gmARRevenuePDFTrackDao;
	/**
	 * This method is used to fetch revenue count by aging and category
	 * @param GmARRevenuePDFTrackModel
	 * @return int
	 */
	public int fetchRevenueTrackCnt(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception{
		return gmARRevenuePDFTrackDao.fetchRevenueTrackCnt(gmARRevenuePDFTrackModel);
	}
	
	/**
	 * This method is used to fetch revenue report by filters 
	 * @param GmARRevenuePDFTrackModel
	 * @return GmARRevenueTrackModel List
	 */	
	@Override
	public List<GmARRevenuePDFTrackModel> fetchRevenueTrackRpt(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		return gmARRevenuePDFTrackDao.fetchRevenueTrackRpt(gmARRevenuePDFTrackModel);
	}

	/**
	 * This method is used to save override PO revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@Override
	public void saveOverridePORevenue(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		gmARRevenuePDFTrackDao.saveOverridePORevenue(gmARRevenuePDFTrackModel);
	}

	/**
	 * This method is used to save override DO revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@Override
	public void saveOverrideDORevenue(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		gmARRevenuePDFTrackDao.saveOverrideDORevenue(gmARRevenuePDFTrackModel);
	}

	/**
	 * This method is used to save comments revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@Override
	public void saveDORevenueComment(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		gmARRevenuePDFTrackDao.saveDORevenueComment(gmARRevenuePDFTrackModel);
	}
	
	/**
	 * This method is used to  approve revenue track  details
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@Override
	public void approveRevenueTrack(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		gmARRevenuePDFTrackDao.approveRevenueTrack(gmARRevenuePDFTrackModel);
	}
}
