package com.globus.erpar.service;

import java.util.List;

import com.globus.erpar.model.GmARDiscrepancyDtlReportModel;
import com.globus.erpar.model.GmARDiscrepancyModel;
import com.globus.erpar.model.GmAROrderPOModel;
import com.globus.erpar.model.GmARPODiscrepancyLineItemModel;

import java.util.Optional;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



public interface GmARDiscrepancyService {
	
	/**
	 * This method is used to fetch discrepancy details Report based on filters
	 * @param gmARDiscrepancyModel
	 * @return gmARDiscrepancyModel
	 */
	public List<GmARDiscrepancyModel> loadDiscrepancyDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception;
	/**
     * This method is used save/update the discrepancy details(collector,category,resolution)
	 * @param GmARDiscrepancyModel
	 * @return GmARDiscrepancyModel
	 */
	public Optional<GmAROrderPOModel> savePODiscrepencyDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception;
	 /**
	  * This method is used fetch discrepancy details based on PO detail Id(collector,category,resolution,remind me for right panel)
	  * @param GmARDiscrepancyModel
	  * @return GmAROrderPOModel
	  */
	public Optional<GmAROrderPOModel> fetchPODiscrepencyIdList(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception;
	 /**
	  * This method is used to save the Remind me days for discrepancy
	  * @param GmARDiscrepancyModel
	  * @return GmAROrderPOModel
	  */
	public Optional<GmAROrderPOModel> saveRemindMeDiscrepencyDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception;
	/**
	  * This method is used to fetch the log details for Category,Collector,Resolution(History Icon)
	  * @param GmARDiscrepancyModel
	  * @return GmARDiscrepancyModel
	  */
	public List<GmARDiscrepancyModel> fetchPOLogDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception;
	
/**
	 * This method is used to fetch line item discrepancy details
	 * @param gmARDiscrepancyModel
	 * @return gmARDiscrepancyModel
	 */
	public List<GmARPODiscrepancyLineItemModel> fetchDiscrepancyLineItems(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception;
	
	/**
	 * This method is used to save line item discrepancy details
	 * @param gmARDiscrepancyModel
	 * @return gmARDiscrepancyModel
	 */
	public String saveDiscrepencyLineItems(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception;
	
	/**
	 * This method is used to remove line item discrepancy details
	 * @param gmARDiscrepancyModel
	 * @return gmARDiscrepancyModel
	 */
	public void removeDiscrepencyLineItems(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception;
	
	/**
	 * This method is used to fetch discrepancy details report
	 * @param gmARDiscrepancyDtlReportModel
	 * @return gmARDiscrepancyDtlReportModel
	 */
	public List<GmARDiscrepancyDtlReportModel> loadDiscrepancyDtlReport(GmARDiscrepancyDtlReportModel gmARDiscrepancyDtlReportModel) throws Exception;
	
}
