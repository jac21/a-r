package com.globus.erpar.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.globus.erpar.dao.GmARInvoiceDao;
import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmARInvoiceModel;
@Service
public class GmARInvoiceServiceImpl implements GmARInvoiceService{

	@Autowired
	GmARInvoiceDao gmARInvoiceDao;
	/**
	 * This method is used to create the batch.
	 * @param String
	 * @return String
	 */
	
	public String saveBatchProcess(GmARInvoiceModel gmARInvoiceModel) throws Exception{
		return gmARInvoiceDao.saveBatchProcess(gmARInvoiceModel);
	}
	/**
	 * This method is used to fetching the orders by PODtlsId for Associated Orders popup
	 */	
	
	@Override
	public List<GmARCustPOModel> fetchOrdersByPO(GmARCustPOModel gmARCustPOModel) throws Exception {
		// TODO Auto-generated method stub
		return gmARInvoiceDao.fetchOrdersByPO(gmARCustPOModel);
	}
	/**
	 * This method is used to fetch PODetails by PODtsId for Associated Orders popup
	 */
	@Override
	public List<GmARCustPOModel> fetchPODetails(GmARCustPOModel gmARCustPOModel) throws Exception {
		// TODO Auto-generated method stub
		return gmARInvoiceDao.fetchPODetails(gmARCustPOModel);
	}
	
	
}
