package com.globus.erpar.service;

import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmAROrderRevenueModel;
import com.globus.erpar.model.GmARRejectPOEmailModel;

import java.util.List;
import java.util.Optional;

/**
 * This GmARCustPOService interface used to load/save the Order Revenue Details,Order PO Details and PO Status. 
 * 
 */
public interface GmARCustPOService {
	/**
	 * This method is used to load the PO report details
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	public List<GmARCustPOModel> fetchPendingPODetails(GmARCustPOModel gmARCustPOModel) throws Exception;
	/**
	 * This method is used to fetch the Order and PO Details for selected Order
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	public List<GmARCustPOModel> fetchOrderDetails(GmARCustPOModel gmARCustPOModel) throws Exception;
	/**
	 * This method is used to fetch the Order Revenue Details for selected Order
	 * @param GmAROrderRevenueModel
	 * @return GmAROrderRevenueModel
	 */
	public GmAROrderRevenueModel fetchOrderRevenueDetails(GmAROrderRevenueModel gmAROrderRevenueModel) throws Exception;
	/**
	 * This method used to save/update Order Revenue Details for selected Order
	 * @param - GmARCustPOModel
	 * @return - GmAROrderRevenueModel
	 */
	public Optional<GmAROrderRevenueModel> saveOrderRevenueDetails(GmARCustPOModel gmARCustPOModel) throws Exception;
	/**
	 * This method is used to save/update Order PO Details
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */	
	public List<GmARCustPOModel> saveOrderPODetails(GmARCustPOModel gmARCustPOModel) throws Exception;
	/**
	 * This method used to validate the current PO with Existing PO
	 * @param - strPONumber,strCompanyId
	 * @return - Count as String
	 */

    public String validatePO(String strPONumber,String strCompanyId,String strOrderId) throws Exception;
    /**
	 * This method is used to fetch the Existing PO Details for entered PO Number
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
    public List<GmARCustPOModel> fetchPODrilldown(GmARCustPOModel gmARCustPOModel) throws Exception;
    /**
	 * This method is used to save/update PO Status(Ready For Invoicing,Move to Discrepancy,Reject)
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	public void saveOrderPOStatus(GmARCustPOModel gmARCustPOModel) throws Exception;
	
	/**
	   * Description: This method is used to get report PO status Report
	   * Author
	   * @param GmARCustPOModel
	   * @return List<GmARCustPOModel>
	   * @exception Exception 
	   **/
	public List<GmARCustPOModel> fetchReadyForInvoiceDtls(GmARCustPOModel gmARCustPOModel) throws Exception;  
	
	/**
	 *  This method used to save/update PO's discrepancy Details for selected PO
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */
	public void saveDiscrepancyDetails(GmARCustPOModel gmARCustPOModel) throws Exception;
	
	/**
	 *  This method used to fetch PO Format drill down details based on account
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */
	public List<GmARCustPOModel> fetchPOFormatDrilldownDetails(GmARCustPOModel gmARCustPOModel) throws Exception;
	
		/**
	 * This method used to fetch Reject PO Notify Email details
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */
	public List<GmARRejectPOEmailModel> fetchRejectPONotifyEmailDtls(GmARRejectPOEmailModel gmARRejectPOEmailModel) throws Exception;
	
	/**
	 * This method used to move PO's into Processor Error for selected PO
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */	
	public void saveProcessorErrorDetails(GmARCustPOModel gmARCustPOModel) throws Exception;
	
	
}
