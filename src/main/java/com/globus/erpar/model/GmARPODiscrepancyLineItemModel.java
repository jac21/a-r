package com.globus.erpar.model;

/**
 * This GmARPODiscrepencyLineItemModel class used to get and set the values for Line Item details of PO. 
 * 
 */

public class GmARPODiscrepancyLineItemModel extends GmARCommonModel {
	
	
	private String strOrderid;
	private String strPnum;
	private String strItemQty;
	private String strItemPrice;
	private String strPOPartNum;
	private String strPOPartQty;
	private String strPOPrice;
	private String strPODtlsId;
	private String strOrdLineItemDtlsId;
	private String strExtDoAmt;
	private String strExtPoAmt;
	private String strPartDesc;
	private String strDescType;
	private String strStatus;
	private String strDescTypeId;
	private String strStatusId;
	
	
	/**
	 * @return the strDescTypeId
	 */
	public String getStrDescTypeId() {
		return strDescTypeId;
	}
	/**
	 * @param strDescTypeId the strDescTypeId to set
	 */
	public void setStrDescTypeId(String strDescTypeId) {
		this.strDescTypeId = strDescTypeId;
	}
	/**
	 * @return the strStatusId
	 */
	public String getStrStatusId() {
		return strStatusId;
	}
	/**
	 * @param strStatusId the strStatusId to set
	 */
	public void setStrStatusId(String strStatusId) {
		this.strStatusId = strStatusId;
	}
	/**
	 * @return the strOrderid
	 */
	public String getStrOrderid() {
		return strOrderid;
	}
	/**
	 * @param strOrderid the strOrderid to set
	 */
	public void setStrOrderid(String strOrderid) {
		this.strOrderid = strOrderid;
	}
	/**
	 * @return the strPnum
	 */
	public String getStrPnum() {
		return strPnum;
	}
	/**
	 * @param strPnum the strPnum to set
	 */
	public void setStrPnum(String strPnum) {
		this.strPnum = strPnum;
	}
	/**
	 * @return the strItemQty
	 */
	public String getStrItemQty() {
		return strItemQty;
	}
	/**
	 * @param strItemQty the strItemQty to set
	 */
	public void setStrItemQty(String strItemQty) {
		this.strItemQty = strItemQty;
	}
	/**
	 * @return the strItemPrice
	 */
	public String getStrItemPrice() {
		return strItemPrice;
	}
	/**
	 * @param strItemPrice the strItemPrice to set
	 */
	public void setStrItemPrice(String strItemPrice) {
		this.strItemPrice = strItemPrice;
	}
	/**
	 * @return the strPOPartNum
	 */
	public String getStrPOPartNum() {
		return strPOPartNum;
	}
	/**
	 * @param strPOPartNum the strPOPartNum to set
	 */
	public void setStrPOPartNum(String strPOPartNum) {
		this.strPOPartNum = strPOPartNum;
	}
	/**
	 * @return the strPOPartQty
	 */
	public String getStrPOPartQty() {
		return strPOPartQty;
	}
	/**
	 * @param strPOPartQty the strPOPartQty to set
	 */
	public void setStrPOPartQty(String strPOPartQty) {
		this.strPOPartQty = strPOPartQty;
	}
	/**
	 * @return the strPOPrice
	 */
	public String getStrPOPrice() {
		return strPOPrice;
	}
	/**
	 * @param strPOPrice the strPOPrice to set
	 */
	public void setStrPOPrice(String strPOPrice) {
		this.strPOPrice = strPOPrice;
	}
	/**
	 * @return the strPODtlsId
	 */
	public String getStrPODtlsId() {
		return strPODtlsId;
	}
	/**
	 * @param strPODtlsId the strPODtlsId to set
	 */
	public void setStrPODtlsId(String strPODtlsId) {
		this.strPODtlsId = strPODtlsId;
	}
	/**
	 * @return the strOrdLineItemDtlsId
	 */
	public String getStrOrdLineItemDtlsId() {
		return strOrdLineItemDtlsId;
	}
	/**
	 * @param strOrdLineItemDtlsId the strOrdLineItemDtlsId to set
	 */
	public void setStrOrdLineItemDtlsId(String strOrdLineItemDtlsId) {
		this.strOrdLineItemDtlsId = strOrdLineItemDtlsId;
	}
	/**
	 * @return the strExtDoAmt
	 */
	public String getStrExtDoAmt() {
		return strExtDoAmt;
	}
	/**
	 * @param strExtDoAmt the strExtDoAmt to set
	 */
	public void setStrExtDoAmt(String strExtDoAmt) {
		this.strExtDoAmt = strExtDoAmt;
	}
	/**
	 * @return the strExtPoAmt
	 */
	public String getStrExtPoAmt() {
		return strExtPoAmt;
	}
	/**
	 * @param strExtPoAmt the strExtPoAmt to set
	 */
	public void setStrExtPoAmt(String strExtPoAmt) {
		this.strExtPoAmt = strExtPoAmt;
	}
	/**
	 * @return the strPartDesc
	 */
	public String getStrPartDesc() {
		return strPartDesc;
	}
	/**
	 * @param strPartDesc the strPartDesc to set
	 */
	public void setStrPartDesc(String strPartDesc) {
		this.strPartDesc = strPartDesc;
	}
	/**
	 * @return the strDescType
	 */
	public String getStrDescType() {
		return strDescType;
	}
	/**
	 * @param strDescType the strDescType to set
	 */
	public void setStrDescType(String strDescType) {
		this.strDescType = strDescType;
	}
	/**
	 * @return the strStatus
	 */
	public String getStrStatus() {
		return strStatus;
	}
	/**
	 * @param strStatus the strStatus to set
	 */
	public void setStrStatus(String strStatus) {
		this.strStatus = strStatus;
	}
	

}
