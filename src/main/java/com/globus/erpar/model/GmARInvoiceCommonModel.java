package com.globus.erpar.model;

import java.util.Date;

public class GmARInvoiceCommonModel {
	
	private String strCompanyId;
	private String strUserId;
	private String inputStr;
	private String strMessage;
	private String strCompanyInfo;
	private String strPlantId;
	private String strLangId;
	private String strPartyId;
	
	
	/**
	 * @return the strCompanyId
	 */
	public String getStrCompanyId() {
		return strCompanyId;
	}
	/**
	 * @param strCompanyId the strCompanyId to set
	 */
	public void setStrCompanyId(String strCompanyId) {
		this.strCompanyId = strCompanyId;
	}
	/**
	 * @return the strUserId
	 */
	public String getStrUserId() {
		return strUserId;
	}
	/**
	 * @param strUserId the strUserId to set
	 */
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}
	/**
	 * @return the inputStr
	 */
	public String getInputStr() {
		return inputStr;
	}
	/**
	 * @param inputStr the inputStr to set
	 */
	public void setInputStr(String inputStr) {
		this.inputStr = inputStr;
	}
	/**
	 * @return the strMessage
	 */
	public String getStrMessage() {
		return strMessage;
	}
	/**
	 * @param strMessage the strMessage to set
	 */
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}
	/**
	 * @return the strCompanyInfo
	 */
	public String getStrCompanyInfo() {
		return strCompanyInfo;
	}
	/**
	 * @param strCompanyInfo the strCompanyInfo to set
	 */
	public void setStrCompanyInfo(String strCompanyInfo) {
		this.strCompanyInfo = strCompanyInfo;
	}
	/**
	 * @return the strPlantId
	 */
	public String getStrPlantId() {
		return strPlantId;
	}
	/**
	 * @param strPlantId the strPlantId to set
	 */
	public void setStrPlantId(String strPlantId) {
		this.strPlantId = strPlantId;
	}
	/**
	 * @return the strLangId
	 */
	public String getStrLangId() {
		return strLangId;
	}
	/**
	 * @param strLangId the strLangId to set
	 */
	public void setStrLangId(String strLangId) {
		this.strLangId = strLangId;
	}
	/**
	 * @return the strPartyId
	 */
	public String getStrPartyId() {
		return strPartyId;
	}
	/**
	 * @param strPartyId the strPartyId to set
	 */
	public void setStrPartyId(String strPartyId) {
		this.strPartyId = strPartyId;
	}
	
	
	
}
