package com.globus.erpar.model;


/**
 * This GmARDiscrepancyDtlReportModel class used to get and set the values of Detail Report of Discrepancy. 
 * 
 */

public class GmARDiscrepancyDtlReportModel extends GmARDiscrepancyModel{
	
	private String strDOPartnum;
	private String strDOPartQty;
	private String strDOItemPrice;
	private String strPOPartNum;
	private String strPOPartQty;
	private String strPOItemPrice;
	private String strExtDoAmt;
	private String strExtPoAmt;
	private String strPartDesc;
	private String strDiscType;
	private String strDiscStatus;
	private String strStatusId;
	
	
	
	public String getStrDOPartnum() {
		return strDOPartnum;
	}
	public void setStrDOPartnum(String strDOPartnum) {
		this.strDOPartnum = strDOPartnum;
	}
	public String getStrDOPartQty() {
		return strDOPartQty;
	}
	public void setStrDOPartQty(String strDOPartQty) {
		this.strDOPartQty = strDOPartQty;
	}
	public String getStrPOItemPrice() {
		return strPOItemPrice;
	}
	public void setStrPOItemPrice(String strPOItemPrice) {
		this.strPOItemPrice = strPOItemPrice;
	}
	public String getStrDOItemPrice() {
		return strDOItemPrice;
	}
	public void setStrDOItemPrice(String strDOItemPrice) {
		this.strDOItemPrice = strDOItemPrice;
	}
	public String getStrPOPartNum() {
		return strPOPartNum;
	}
	public void setStrPOPartNum(String strPOPartNum) {
		this.strPOPartNum = strPOPartNum;
	}
	public String getStrPOPartQty() {
		return strPOPartQty;
	}
	public void setStrPOPartQty(String strPOPartQty) {
		this.strPOPartQty = strPOPartQty;
	}

	public String getStrExtDoAmt() {
		return strExtDoAmt;
	}
	public void setStrExtDoAmt(String strExtDoAmt) {
		this.strExtDoAmt = strExtDoAmt;
	}
	public String getStrExtPoAmt() {
		return strExtPoAmt;
	}
	public void setStrExtPoAmt(String strExtPoAmt) {
		this.strExtPoAmt = strExtPoAmt;
	}
	public String getStrPartDesc() {
		return strPartDesc;
	}
	public void setStrPartDesc(String strPartDesc) {
		this.strPartDesc = strPartDesc;
	}
	
	public String getStrDiscType() {
		return strDiscType;
	}
	public void setStrDiscType(String strDiscType) {
		this.strDiscType = strDiscType;
	}
	public String getStrDiscStatus() {
		return strDiscStatus;
	}
	public void setStrDiscStatus(String strDiscStatus) {
		this.strDiscStatus = strDiscStatus;
	}
	public String getStrStatusId() {
		return strStatusId;
	}
	public void setStrStatusId(String strStatusId) {
		this.strStatusId = strStatusId;
	}


}
