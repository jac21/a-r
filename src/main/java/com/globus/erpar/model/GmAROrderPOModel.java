package com.globus.erpar.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This GmAROrderPOModel class used to get and set the values for Order PO Detail
 * 
 */
@Entity
@Table(name="t5001_order_po_dtls")

public class GmAROrderPOModel extends GmARCommonModel{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column( name = "C5001_ORDER_PO_DTLS_ID")
	private int strOrderPODtlId;
	@Column ( name = "C501_ORDER_ID")
	private String strOrderId;
	@Column ( name = "C704_ACCOUNT_ID")
	private String strAccountId;
	@Column ( name = "C5001_CUST_PO_ID")
	private String strCustPoId;
	@Column ( name = "C5001_CUST_PO_AMT")
	private String strCustPoAmt;
	@Column ( name = "C5001_CUST_PO_DATE")
	private Date strCustPoDate;
	@Column ( name = "C5001_CUST_PO_UPDATED_BY")
	private String strCustPoUpdatedBy;
	@Column ( name = "C901_CUST_PO_STATUS")
	private String strCustPoStatus;
	@Column ( name = "C5001_PO_STATUS_UPDATED_BY")
	private String strPoStatusUpdatedBY;
	@Column ( name = "C5001_PO_STATUS_UPDATED_DATE")
	private Date strPoStatusUpdatedDate;
	@Column ( name = "C5001_PO_STATUS_HISTORY_FL")
	private String strPoStatusHistoryFl;
	@Column ( name = "C5001_PO_MOVED_DISCREPANCY_FL")
	private String strPoMovedDiscrepancyFl;
	@Column ( name = "C501_PO_MOVE_DISCREPANCY_BY")
	private String strPoMoveDiscrepancyBy;
	@Column ( name = "C501_PO_MOVE_DISCREPANCY_DATE")
	private Date strPoMoveDiscrepancyDate;
	@Column ( name = "C901_COLLECTOR_ID")
	private String strCollectorId;
	@Column ( name = "C5001_COLLECOTER_HISTORY_FL")
	private String strCollectorHistoryFl;
	@Column ( name = "C5001_PO_DISC_REMAIND_DAYS")
	private String strPoDiscRemaindDays;
	@Column ( name = "C5001_PO_DISC_REMAIND_BY")
	private String strPoDiscRemaindBy;
	@Column ( name = "C5001_PO_DISC_REMAIND_DATE")
	private Date strPoDiscRemaindDate;
	@Column ( name = "C901_PO_DISC_CATEGORY_TYPE")
	private String strPoDiscCategoryType;
	@Column ( name = "C901_PO_DISC_RESOLUTION_TYPE")
	private String strPoDiscResolutionType;
	@Column ( name = "C5001_PO_DISC_CATEGORY_HISTORY_FL")
	private String strPoDiscCategoryHisFl;
	@Column ( name = "C5001_PO_DISC_RESOLUTION_HISTORY_FL")
	private String strPoDiscResolutionHisFl;
	@Column ( name = "C5001_RESOLVED_DISC_BY")
	private String strResovedDiscBy;
	@Column ( name = "C5001_RESOLVED_DISC_DATE")
	private Date strResolvedDiscDate;
	@Column ( name = "C5001_DESC_ESC_BY")
	private String strPoDis;
	@Column ( name = "C9600_BATCH_ID")
	private String strBatchId;
	@Column ( name = "C5001_PO_DISC_BY_LINE_ITEM_FL")
	private String strPoDiscByLineItemFl;
	@Column ( name = "C5001_VOID_FL")
	private String strVoidFl;
	@Column ( name = "C5001_LAST_UPDATED_BY")
	private String strLastUpdatedBy;
	@Column ( name = "C5001_LAST_UPDATED_DATE")
	private Date strLastUpdatedDate;
	
	
	public int getStrOrderPODtlId() {
		return strOrderPODtlId;
	}
	public void setStrOrderPODtlId(int strOrderPODtlId) {
		this.strOrderPODtlId = strOrderPODtlId;
	}
	public String getStrOrderId() {
		return strOrderId;
	}
	public void setStrOrderId(String strOrderId) {
		this.strOrderId = strOrderId;
	}
	public String getStrAccountId() {
		return strAccountId;
	}
	public void setStrAccountId(String strAccountId) {
		this.strAccountId = strAccountId;
	}
	public String getStrCustPoId() {
		return strCustPoId;
	}
	public void setStrCustPoId(String strCustPoId) {
		this.strCustPoId = strCustPoId;
	}
	public String getStrCustPoAmt() {
		return strCustPoAmt;
	}
	public void setStrCustPoAmt(String strCustPoAmt) {
		this.strCustPoAmt = strCustPoAmt;
	}
	public Date getStrCustPoDate() {
		return strCustPoDate;
	}
	public void setStrCustPoDate(Date strCustPoDate) {
		this.strCustPoDate = strCustPoDate;
	}
	public String getStrCustPoUpdatedBy() {
		return strCustPoUpdatedBy;
	}
	public void setStrCustPoUpdatedBy(String strCustPoUpdatedBy) {
		this.strCustPoUpdatedBy = strCustPoUpdatedBy;
	}
	public String getStrCustPoStatus() {
		return strCustPoStatus;
	}
	public void setStrCustPoStatus(String strCustPoStatus) {
		this.strCustPoStatus = strCustPoStatus;
	}
	public String getStrPoStatusUpdatedBY() {
		return strPoStatusUpdatedBY;
	}
	public void setStrPoStatusUpdatedBY(String strPoStatusUpdatedBY) {
		this.strPoStatusUpdatedBY = strPoStatusUpdatedBY;
	}
	public Date getStrPoStatusUpdatedDate() {
		return strPoStatusUpdatedDate;
	}
	public void setStrPoStatusUpdatedDate(Date strPoStatusUpdatedDate) {
		this.strPoStatusUpdatedDate = strPoStatusUpdatedDate;
	}
	public String getStrPoStatusHistoryFl() {
		return strPoStatusHistoryFl;
	}
	public void setStrPoStatusHistoryFl(String strPoStatusHistoryFl) {
		this.strPoStatusHistoryFl = strPoStatusHistoryFl;
	}
	public String getStrPoMovedDiscrepancyFl() {
		return strPoMovedDiscrepancyFl;
	}
	public void setStrPoMovedDiscrepancyFl(String strPoMovedDiscrepancyFl) {
		this.strPoMovedDiscrepancyFl = strPoMovedDiscrepancyFl;
	}
	public String getStrPoMoveDiscrepancyBy() {
		return strPoMoveDiscrepancyBy;
	}
	public void setStrPoMoveDiscrepancyBy(String strPoMoveDiscrepancyBy) {
		this.strPoMoveDiscrepancyBy = strPoMoveDiscrepancyBy;
	}
	public Date getStrPoMoveDiscrepancyDate() {
		return strPoMoveDiscrepancyDate;
	}
	public void setStrPoMoveDiscrepancyDate(Date strPoMoveDiscrepancyDate) {
		this.strPoMoveDiscrepancyDate = strPoMoveDiscrepancyDate;
	}
	public String getStrCollectorId() {
		return strCollectorId;
	}
	public void setStrCollectorId(String strCollectorId) {
		this.strCollectorId = strCollectorId;
	}
	public String getStrCollectorHistoryFl() {
		return strCollectorHistoryFl;
	}
	public void setStrCollectorHistoryFl(String strCollectorHistoryFl) {
		this.strCollectorHistoryFl = strCollectorHistoryFl;
	}
	public String getStrPoDiscRemaindDays() {
		return strPoDiscRemaindDays;
	}
	public void setStrPoDiscRemaindDays(String strPoDiscRemaindDays) {
		this.strPoDiscRemaindDays = strPoDiscRemaindDays;
	}
	public String getStrPoDiscRemaindBy() {
		return strPoDiscRemaindBy;
	}
	public void setStrPoDiscRemaindBy(String strPoDiscRemaindBy) {
		this.strPoDiscRemaindBy = strPoDiscRemaindBy;
	}
	public Date getStrPoDiscRemaindDate() {
		return strPoDiscRemaindDate;
	}
	public void setStrPoDiscRemaindDate(Date strPoDiscRemaindDate) {
		this.strPoDiscRemaindDate = strPoDiscRemaindDate;
	}
	public String getStrPoDiscCategoryType() {
		return strPoDiscCategoryType;
	}
	public void setStrPoDiscCategoryType(String strPoDiscCategoryType) {
		this.strPoDiscCategoryType = strPoDiscCategoryType;
	}
	public String getStrPoDiscResolutionType() {
		return strPoDiscResolutionType;
	}
	public void setStrPoDiscResolutionType(String strPoDiscResolutionType) {
		this.strPoDiscResolutionType = strPoDiscResolutionType;
	}
	public String getStrPoDiscCategoryHisFl() {
		return strPoDiscCategoryHisFl;
	}
	public void setStrPoDiscCategoryHisFl(String strPoDiscCategoryHisFl) {
		this.strPoDiscCategoryHisFl = strPoDiscCategoryHisFl;
	}
	public String getStrPoDiscResolutionHisFl() {
		return strPoDiscResolutionHisFl;
	}
	public void setStrPoDiscResolutionHisFl(String strPoDiscResolutionHisFl) {
		this.strPoDiscResolutionHisFl = strPoDiscResolutionHisFl;
	}
	public String getStrResovedDiscBy() {
		return strResovedDiscBy;
	}
	public void setStrResovedDiscBy(String strResovedDiscBy) {
		this.strResovedDiscBy = strResovedDiscBy;
	}
	public Date getStrResolvedDiscDate() {
		return strResolvedDiscDate;
	}
	public void setStrResolvedDiscDate(Date strResolvedDiscDate) {
		this.strResolvedDiscDate = strResolvedDiscDate;
	}
	public String getStrPoDis() {
		return strPoDis;
	}
	public void setStrPoDis(String strPoDis) {
		this.strPoDis = strPoDis;
	}
	public String getStrBatchId() {
		return strBatchId;
	}
	public void setStrBatchId(String strBatchId) {
		this.strBatchId = strBatchId;
	}
	public String getStrPoDiscByLineItemFl() {
		return strPoDiscByLineItemFl;
	}
	public void setStrPoDiscByLineItemFl(String strPoDiscByLineItemFl) {
		this.strPoDiscByLineItemFl = strPoDiscByLineItemFl;
	}
	public String getStrVoidFl() {
		return strVoidFl;
	}
	public void setStrVoidFl(String strVoidFl) {
		this.strVoidFl = strVoidFl;
	}
	public String getStrLastUpdatedBy() {
		return strLastUpdatedBy;
	}
	public void setStrLastUpdatedBy(String strLastUpdatedBy) {
		this.strLastUpdatedBy = strLastUpdatedBy;
	}
	public Date getStrLastUpdatedDate() {
		return strLastUpdatedDate;
	}
	public void setStrLastUpdatedDate(Date strLastUpdatedDate) {
		this.strLastUpdatedDate = strLastUpdatedDate;
	}
	

}
