package com.globus.erpar.model;

/**
 * This GmARRejectPOEmailModel class used to set & get values for reject PO Email Details
 *  
 */

public class GmARRejectPOEmailModel extends GmARCustPOModel{
	
	private String strPORejectTOEmail;
	private String strPORejectAccID;
	private String strPORejectAccNm;
	private String strPORejectBy;
	private String strPORejectCCEmail;
	private String strPORejectFromEmail;
	private String strPORejectSub;
	private String strPORejectConType;
	private String strPORejectTitle;
	private String strPORejectBody;
	private String strPORejectHeaderFl;

	
	private String strComment;
	private String strCancelId;
	private String strCanceIdNm;
	private String strPORejectedDt;
	private String strPOLocalDt;
	private String strPOAccidAndNm;
	
	

	
	
	public void setStrPORejectTOEmail(String strPORejectTOEmail) {
		this.strPORejectTOEmail = strPORejectTOEmail;
	}
	public String getStrPORejectAccID() {
		return strPORejectAccID;
	}
	public void setStrPORejectAccID(String strPORejectAccID) {
		this.strPORejectAccID = strPORejectAccID;
	}
	public String getStrPORejectAccNm() {
		return strPORejectAccNm;
	}
	public void setStrPORejectAccNm(String strPORejectAccNm) {
		this.strPORejectAccNm = strPORejectAccNm;
	}
	public String getStrPORejectBy() {
		return strPORejectBy;
	}
	public void setStrPORejectBy(String strPORejectBy) {
		this.strPORejectBy = strPORejectBy;
	}
	public String getStrPORejectCCEmail() {
		return strPORejectCCEmail;
	}
	public void setStrPORejectCCEmail(String strPORejectCCEmail) {
		this.strPORejectCCEmail = strPORejectCCEmail;
	}
	public String getStrPORejectFromEmail() {
		return strPORejectFromEmail;
	}
	public void setStrPORejectFromEmail(String strPORejectFromEmail) {
		this.strPORejectFromEmail = strPORejectFromEmail;
	}
	public String getStrPORejectSub() {
		return strPORejectSub;
	}
	public void setStrPORejectSub(String strPORejectSub) {
		this.strPORejectSub = strPORejectSub;
	}
	public String getStrPORejectConType() {
		return strPORejectConType;
	}
	public void setStrPORejectConType(String strPORejectConType) {
		this.strPORejectConType = strPORejectConType;
	}
	public String getStrPORejectTitle() {
		return strPORejectTitle;
	}
	public void setStrPORejectTitle(String strPORejectTitle) {
		this.strPORejectTitle = strPORejectTitle;
	}
	public String getStrPORejectBody() {
		return strPORejectBody;
	}
	public void setStrPORejectBody(String strPORejectBody) {
		this.strPORejectBody = strPORejectBody;
	}

	public String getStrComment() {
		return strComment;
	}
	public void setStrComment(String strComment) {
		this.strComment = strComment;
	}

	public String getStrCancelId() {
		return strCancelId;
	}
	public void setStrCancelId(String strCancelId) {
		this.strCancelId = strCancelId;
	}
	public String getStrCanceIdNm() {
		return strCanceIdNm;
	}
	public void setStrCanceIdNm(String strCanceIdNm) {
		this.strCanceIdNm = strCanceIdNm;
	}
	public String getStrPORejectedDt() {
		return strPORejectedDt;
	}
	public void setStrPORejectedDt(String strPORejectedDt) {
		this.strPORejectedDt = strPORejectedDt;
	}
	public String getStrPORejectHeaderFl() {
		return strPORejectHeaderFl;
	}
	public void setStrPORejectHeaderFl(String strPORejectHeaderFl) {
		this.strPORejectHeaderFl = strPORejectHeaderFl;
	}
	public String getStrPOLocalDt() {
		return strPOLocalDt;
	}
	public void setStrPOLocalDt(String strPOLocalDt) {
		this.strPOLocalDt = strPOLocalDt;
	}
	public String getStrPORejectTOEmail() {
		return strPORejectTOEmail;
	}
	public String getStrPOAccidAndNm() {
		return strPOAccidAndNm;
	}
	public void setStrPOAccidAndNm(String strPOAccidAndNm) {
		this.strPOAccidAndNm = strPOAccidAndNm;
	}
	

}
