package com.globus.erpar.model;


/**
 * This GmARRevenueTrackPDFModel class used to set & get values for revenue tracking model
 *  
 */
public class GmARRevenuePDFTrackModel extends GmARCommonModel{
        
	    private int revenueTrackId;
		private String strAging;
		private String strCategory;
		private String strCompZone;
		private String strPlantId;

		private String strOrderId;
		private String strOrderdate;
		private String strContractFlag;
		private String strState;
		private String strPoRevenue;
		private String strDoRevenue;
		private String strOverideDo;
		private String strComments;
		private String strAgingNm;
		private String strApproveIds;
		private boolean chkFl=true;
		private String strOrdStartDate;
		private String strOrdEndDate;
		private String strCustomcss;
		private String strOveridePo;
		/**
		 * @return the revenueTrackId
		 */
		public int getRevenueTrackId() {
			return revenueTrackId;
		}
		/**
		 * @param revenueTrackId the revenueTrackId to set
		 */
		public void setRevenueTrackId(int revenueTrackId) {
			this.revenueTrackId = revenueTrackId;
		}
		/**
		 * @return the strAging
		 */
		public String getStrAging() {
			return strAging;
		}
		/**
		 * @param strAging the strAging to set
		 */
		public void setStrAging(String strAging) {
			this.strAging = strAging;
		}
		/**
		 * @return the strCategory
		 */
		public String getStrCategory() {
			return strCategory;
		}
		/**
		 * @param strCategory the strCategory to set
		 */
		public void setStrCategory(String strCategory) {
			this.strCategory = strCategory;
		}
		/**
		 * @return the strCompZone
		 */
		public String getStrCompZone() {
			return strCompZone;
		}
		/**
		 * @param strCompZone the strCompZone to set
		 */
		public void setStrCompZone(String strCompZone) {
			this.strCompZone = strCompZone;
		}
		/**
		 * @return the strPlantId
		 */
		public String getStrPlantId() {
			return strPlantId;
		}
		/**
		 * @param strPlantId the strPlantId to set
		 */
		public void setStrPlantId(String strPlantId) {
			this.strPlantId = strPlantId;
		}
		/**
		 * @return the strOrderId
		 */
		public String getStrOrderId() {
			return strOrderId;
		}
		/**
		 * @param strOrderId the strOrderId to set
		 */
		public void setStrOrderId(String strOrderId) {
			this.strOrderId = strOrderId;
		}
		/**
		 * @return the strOrderdate
		 */
		public String getStrOrderdate() {
			return strOrderdate;
		}
		/**
		 * @param strOrderdate the strOrderdate to set
		 */
		public void setStrOrderdate(String strOrderdate) {
			this.strOrderdate = strOrderdate;
		}
		/**
		 * @return the strContractFlag
		 */
		public String getStrContractFlag() {
			return strContractFlag;
		}
		/**
		 * @param strContractFlag the strContractFlag to set
		 */
		public void setStrContractFlag(String strContractFlag) {
			this.strContractFlag = strContractFlag;
		}
		/**
		 * @return the strState
		 */
		public String getStrState() {
			return strState;
		}
		/**
		 * @param strState the strState to set
		 */
		public void setStrState(String strState) {
			this.strState = strState;
		}
		/**
		 * @return the strPoRevenue
		 */
		public String getStrPoRevenue() {
			return strPoRevenue;
		}
		/**
		 * @param strPoRevenue the strPoRevenue to set
		 */
		public void setStrPoRevenue(String strPoRevenue) {
			this.strPoRevenue = strPoRevenue;
		}
		/**
		 * @return the strDoRevenue
		 */
		public String getStrDoRevenue() {
			return strDoRevenue;
		}
		/**
		 * @param strDoRevenue the strDoRevenue to set
		 */
		public void setStrDoRevenue(String strDoRevenue) {
			this.strDoRevenue = strDoRevenue;
		}
		/**
		 * @return the strOverideDo
		 */
		public String getStrOverideDo() {
			return strOverideDo;
		}
		/**
		 * @param strOverideDo the strOverideDo to set
		 */
		public void setStrOverideDo(String strOverideDo) {
			this.strOverideDo = strOverideDo;
		}
		/**
		 * @return the strComments
		 */
		public String getStrComments() {
			return strComments;
		}
		/**
		 * @param strComments the strComments to set
		 */
		public void setStrComments(String strComments) {
			this.strComments = strComments;
		}
		/**
		 * @return the strAgingNm
		 */
		public String getStrAgingNm() {
			return strAgingNm;
		}
		/**
		 * @param strAgingNm the strAgingNm to set
		 */
		public void setStrAgingNm(String strAgingNm) {
			this.strAgingNm = strAgingNm;
		}
		/**
		 * @return the strApproveIds
		 */
		public String getStrApproveIds() {
			return strApproveIds;
		}
		/**
		 * @param strApproveIds the strApproveIds to set
		 */
		public void setStrApproveIds(String strApproveIds) {
			this.strApproveIds = strApproveIds;
		}
		/**
		 * @return the chkFl
		 */
		public boolean isChkFl() {
			return chkFl;
		}
		/**
		 * @param chkFl the chkFl to set
		 */
		public void setChkFl(boolean chkFl) {
			this.chkFl = chkFl;
		}
		/**
		 * @return the strOrdStartDate
		 */
		public String getStrOrdStartDate() {
			return strOrdStartDate;
		}
		/**
		 * @param strOrdStartDate the strOrdStartDate to set
		 */
		public void setStrOrdStartDate(String strOrdStartDate) {
			this.strOrdStartDate = strOrdStartDate;
		}
		/**
		 * @return the strOrdEndDate
		 */
		public String getStrOrdEndDate() {
			return strOrdEndDate;
		}
		/**
		 * @param strOrdEndDate the strOrdEndDate to set
		 */
		public void setStrOrdEndDate(String strOrdEndDate) {
			this.strOrdEndDate = strOrdEndDate;
		}
		/**
		 * @return the strCustomcss
		 */
		public String getStrCustomcss() {
			return strCustomcss;
		}
		/**
		 * @param strCustomcss the strCustomcss to set
		 */
		public void setStrCustomcss(String strCustomcss) {
			this.strCustomcss = strCustomcss;
		}
		/**
		 * @return the strOveridePo
		 */
		public String getStrOveridePo() {
			return strOveridePo;
		}
		/**
		 * @param strOveridePo the strOveridePo to set
		 */
		public void setStrOveridePo(String strOveridePo) {
			this.strOveridePo = strOveridePo;
		}
		
	
		
	
		
}
