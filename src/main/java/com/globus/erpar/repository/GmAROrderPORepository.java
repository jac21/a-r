package com.globus.erpar.repository;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.globus.erpar.model.GmAROrderPOModel;

/**
 * This GmAROrderPORepository class used to get the OrderPo Details Value . 
 * 
 */
public interface GmAROrderPORepository extends JpaRepository<GmAROrderPOModel, Integer>{
	
	Optional<GmAROrderPOModel>  findByStrOrderPODtlIdAndStrVoidFlIsNull(int strOrderPODtlId);

}








