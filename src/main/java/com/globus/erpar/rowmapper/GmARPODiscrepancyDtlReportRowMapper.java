package com.globus.erpar.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARDiscrepancyDtlReportModel;
import com.globus.erpar.model.GmARPODiscrepancyLineItemModel;


/**
 * This GmARPODiscrepancyDtlReportRowMapper class used to map the Detail Report of Discrepancy.
 * 
 */
public class GmARPODiscrepancyDtlReportRowMapper implements RowMapper<GmARDiscrepancyDtlReportModel>{
	
	@Autowired
	GmCommonClass gmCommonClass;
	
	/**
	 * This mapRow method used to set the Detail Report values to GmARDiscrepancyDtlReportModel
	 * 
	 */
	
	@Override
	public GmARDiscrepancyDtlReportModel mapRow(ResultSet resultSet, int i) throws SQLException {
		
		GmARDiscrepancyDtlReportModel gmARDiscrepancyDtlReportModel = new GmARDiscrepancyDtlReportModel();
		gmARDiscrepancyDtlReportModel.setStrPONumber(GmCommonClass.parseNull(resultSet.getString("strPONumber")));
		gmARDiscrepancyDtlReportModel.setStrCategoryNm(GmCommonClass.parseNull(resultSet.getString("strCategoryNm")));
		gmARDiscrepancyDtlReportModel.setStrCollectorNm(GmCommonClass.parseNull(resultSet.getString("strCollectorNm")));
		gmARDiscrepancyDtlReportModel.setStrOrderId(GmCommonClass.parseNull(resultSet.getString("strOrderid")));
		gmARDiscrepancyDtlReportModel.setStrDOPartnum(GmCommonClass.parseNull(resultSet.getString("strDOPartnum")));
		gmARDiscrepancyDtlReportModel.setStrPartDesc(GmCommonClass.parseNull(resultSet.getString("strPartDesc")));	
		gmARDiscrepancyDtlReportModel.setStrDOPartQty(GmCommonClass.parseNull(resultSet.getString("strDOPartQty")));
		gmARDiscrepancyDtlReportModel.setStrDOItemPrice(GmCommonClass.parseNull(resultSet.getString("strDOItemPrice")));
		gmARDiscrepancyDtlReportModel.setStrExtDoAmt(GmCommonClass.parseNull(resultSet.getString("strExtDoAmt")));
		gmARDiscrepancyDtlReportModel.setStrPOPartNum(GmCommonClass.parseNull(resultSet.getString("strPOPartNum")));
		gmARDiscrepancyDtlReportModel.setStrPOPartQty(GmCommonClass.parseNull(resultSet.getString("strPOPartQty")));
		gmARDiscrepancyDtlReportModel.setStrPOItemPrice(GmCommonClass.parseNull(resultSet.getString("strPOItemPrice")));
		gmARDiscrepancyDtlReportModel.setStrExtPoAmt(GmCommonClass.parseNull(resultSet.getString("strExtPoAmt")));
		gmARDiscrepancyDtlReportModel.setStrSalesRep(GmCommonClass.parseNull(resultSet.getString("strSalesRep")));
		gmARDiscrepancyDtlReportModel.setStrRepAcc(GmCommonClass.parseNull(resultSet.getString("strRepAcc")));
		gmARDiscrepancyDtlReportModel.setStrDiscType(GmCommonClass.parseNull(resultSet.getString("strDiscType")));
		gmARDiscrepancyDtlReportModel.setStrDiscStatus(GmCommonClass.parseNull(resultSet.getString("strDiscStatus")));
		gmARDiscrepancyDtlReportModel.setStrResDate(GmCommonClass.parseNull(resultSet.getString("strResDate")));
		gmARDiscrepancyDtlReportModel.setStrDisdate(GmCommonClass.parseNull(resultSet.getString("strDisDate")));
		
		
		return gmARDiscrepancyDtlReportModel;
	}

}
