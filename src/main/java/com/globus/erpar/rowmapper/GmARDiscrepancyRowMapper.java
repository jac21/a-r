package com.globus.erpar.rowmapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARDiscrepancyModel;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * This GmARDiscrepancyRowMapper class used to map the Discrepancy details of PO.
 * 
 */

public class GmARDiscrepancyRowMapper implements RowMapper<GmARDiscrepancyModel>{
	
	@Autowired
	GmCommonClass gmCommonClass;

	@Override
	public GmARDiscrepancyModel mapRow(ResultSet resultSet, int i) throws SQLException {
		GmARDiscrepancyModel gmARDiscrepancyModel = new GmARDiscrepancyModel();
		gmARDiscrepancyModel.setStrOrderId(GmCommonClass.parseNull(resultSet.getString("strOrderId")));
		gmARDiscrepancyModel.setStrCustPONumber(GmCommonClass.parseNull(resultSet.getString("strCustPO")));
		gmARDiscrepancyModel.setStrTotal(GmCommonClass.parseNull(resultSet.getString("strOrderAmt")));
		gmARDiscrepancyModel.setStrPOAmt(GmCommonClass.parseNull(resultSet.getString("strPOAmt")));
		gmARDiscrepancyModel.setStrRepAcc(GmCommonClass.parseNull(resultSet.getString("strRepAccNm")));
		gmARDiscrepancyModel.setStrSalesRep(GmCommonClass.parseNull(resultSet.getString("strSalesRepNm")));
		gmARDiscrepancyModel.setStrCategoryNm(GmCommonClass.parseNull(resultSet.getString("strCategoryNm")));
		gmARDiscrepancyModel.setStrDiffAmt(GmCommonClass.parseNull(resultSet.getString("strDiffAmt")));
		gmARDiscrepancyModel.setStrAccid(GmCommonClass.parseNull(resultSet.getString("strAccId")));
		gmARDiscrepancyModel.setStrParentAcc(GmCommonClass.parseNull(resultSet.getString("strParentAcc")));
		gmARDiscrepancyModel.setStrParentOrdId(GmCommonClass.parseNull(resultSet.getString("strParentOrdId")));
		gmARDiscrepancyModel.setStrResDate(GmCommonClass.parseNull(resultSet.getString("strResDate")));
		gmARDiscrepancyModel.setStrDisdate(GmCommonClass.parseNull(resultSet.getString("strDisDate")));
		gmARDiscrepancyModel.setStrCollectorNm(GmCommonClass.parseNull(resultSet.getString("strCollectorNm")));
		gmARDiscrepancyModel.setStrPONumDtlsId(GmCommonClass.parseNull(resultSet.getString("strPODetaiId")));
		gmARDiscrepancyModel.setStrOrdDT(GmCommonClass.parseNull(resultSet.getString("strOrdDT")));
		gmARDiscrepancyModel.setStrADName(GmCommonClass.parseNull(resultSet.getString("strADName")));
		gmARDiscrepancyModel.setStrVPName(GmCommonClass.parseNull(resultSet.getString("strVPName")));
		gmARDiscrepancyModel.setStrDefaultCollector(GmCommonClass.parseNull(resultSet.getString("collectorid")));
		gmARDiscrepancyModel.setStrOrdReceiveMode(GmCommonClass.parseNull(resultSet.getString("ordMode")));
		gmARDiscrepancyModel.setStrPOResolvefl(GmCommonClass.parseNull(resultSet.getString("strPOResolvefl")));
		gmARDiscrepancyModel.setStrPODiscRemdate(GmCommonClass.parseNull(resultSet.getString("strPODiscRemdate")));
		gmARDiscrepancyModel.setStrPOEnteredDate(GmCommonClass.parseNull(resultSet.getString("strPOEnteredDt")));
		gmARDiscrepancyModel.setStrCustPOUpdatedBy(GmCommonClass.parseNull(resultSet.getString("strPOEnteredBy")));
		return gmARDiscrepancyModel;
	}
	
}
