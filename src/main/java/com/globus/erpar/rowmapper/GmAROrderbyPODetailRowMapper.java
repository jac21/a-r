package com.globus.erpar.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARCustPOModel;

public class GmAROrderbyPODetailRowMapper implements RowMapper<GmARCustPOModel>{
	@Autowired
	GmCommonClass gmCommonClass;
	
	//This is to map the required column from the result set for Associated orders modal pop up body
	public GmARCustPOModel mapRow(ResultSet resultSet, int i) throws SQLException {
		GmARCustPOModel gmARCustPOModel = new GmARCustPOModel();
		gmARCustPOModel.setStrOrderId(GmCommonClass.parseNull(resultSet.getString("ORDID")));
		gmARCustPOModel.setStrOrderTypeName(GmCommonClass.parseNull(resultSet.getString("ORDTYP")));
		gmARCustPOModel.setStrTotal(GmCommonClass.parseNull(resultSet.getString("TOTCOST")));
		return gmARCustPOModel;
		
		
	}
	}

