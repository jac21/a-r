package com.globus.erpar.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARCustPOModel;

/**
 * This GmARCustPOFormatRowMapper class used to map the PO Format Details.
 * 
 */
public class GmARCustPOFormatRowMapper implements RowMapper<GmARCustPOModel> {
	
	@Autowired
	GmCommonClass gmCommonClass;
	
	/**
	 * This mapRow method used to set the map the PO Format Details GmARCustPOModel
	 * 
	 */
	
	@Override
	public GmARCustPOModel mapRow(ResultSet resultSet, int i) throws SQLException {
		
		GmARCustPOModel gmARCustPOModel = new GmARCustPOModel();
		
		gmARCustPOModel.setStrParentAcc(GmCommonClass.parseNull(resultSet.getString("strPartyName")));
		gmARCustPOModel.setStrAccid(GmCommonClass.parseNull(resultSet.getString("strAccId")));
		gmARCustPOModel.setStrInvoice(GmCommonClass.parseNull(resultSet.getString("strInvoiceId")));
		gmARCustPOModel.setStrInvoiceDt(GmCommonClass.parseNull(resultSet.getString("strInvoiceDt")));
		gmARCustPOModel.setStrCustPONumber(GmCommonClass.parseNull(resultSet.getString("strCustomerPO")));
		gmARCustPOModel.setStrInvoiceAmt(GmCommonClass.parseNull(resultSet.getString("strInvoiceAmt")));
		
		return gmARCustPOModel;
	}

}
