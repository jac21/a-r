package com.globus.erpar.rowmapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import jdk.nashorn.internal.ir.SetSplitState;

import com.globus.common.util.GmCommonClass;
import org.springframework.beans.factory.annotation.Autowired;
import com.globus.erpar.model.GmARCustPOModel;
/**
 * This GmPORowMapper class used to map the Order Revenue Details,Order PO Details and PO Status. 
 * 
 */

public class GmPORowMapper implements RowMapper<GmARCustPOModel>{
	@Autowired
	GmCommonClass gmCommonClass;
	
	@Override
	public GmARCustPOModel mapRow(ResultSet resultSet, int i) throws SQLException {
		GmARCustPOModel gmARCustPOModel = new GmARCustPOModel();
		gmARCustPOModel.setStrRepAcc(GmCommonClass.parseNull(resultSet.getString("strRepAcc")));
		gmARCustPOModel.setStrOrderId(GmCommonClass.parseNull(resultSet.getString("strOrderId")));
		gmARCustPOModel.setStrOrdDT(GmCommonClass.parseNull(resultSet.getString("strOrdDT")));
		gmARCustPOModel.setStrTotal(GmCommonClass.parseNull(resultSet.getString("strTotal")));
		gmARCustPOModel.setStrSalesRep(GmCommonClass.parseNull(resultSet.getString("strSalesRep")));
		gmARCustPOModel.setStrFieldSales(GmCommonClass.parseNull(resultSet.getString("strFieldSales")));
		gmARCustPOModel.setStrParentAcc(GmCommonClass.parseNull(resultSet.getString("strParentAcc")));
		gmARCustPOModel.setStrOrdertype(GmCommonClass.parseNull(resultSet.getString("strOrdertype")));
		gmARCustPOModel.setStrOrderId(GmCommonClass.parseNull(resultSet.getString("strOrderId")));
		gmARCustPOModel.setStrHoldFl(GmCommonClass.parseNull(resultSet.getString("strHoldFl")));
		gmARCustPOModel.setStrSurgeryDt(GmCommonClass.parseNull(resultSet.getString("strSurgeryDt"))); 
		gmARCustPOModel.setStrADName(GmCommonClass.parseNull(resultSet.getString("strADName")));
		gmARCustPOModel.setStrVPName(GmCommonClass.parseNull(resultSet.getString("strVPName")));
		gmARCustPOModel.setStrAccid(GmCommonClass.parseNull(resultSet.getString("strAccId")));
		gmARCustPOModel.setStrParentOrdId(GmCommonClass.parseNull(resultSet.getString("strParentOrdId")));
		gmARCustPOModel.setStrPONumber(GmCommonClass.parseNull(resultSet.getString("strPONumber")));
		gmARCustPOModel.setStrOrdReceiveMode(GmCommonClass.parseNull(resultSet.getString("strOrdMode")));
		gmARCustPOModel.setStrParentAccId(GmCommonClass.parseNull(resultSet.getString("strParentAccId")));
		gmARCustPOModel.setStrPOEnteredDate(GmCommonClass.parseNull(resultSet.getString("strPOEnteredDt")));
		gmARCustPOModel.setStrCustPOUpdatedBy(GmCommonClass.parseNull(resultSet.getString("strPOEnteredBy")));
	
		return gmARCustPOModel;
	}
}
