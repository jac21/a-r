package com.globus.erpar.rowmapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.globus.common.util.GmCommonClass;
import org.springframework.beans.factory.annotation.Autowired;
import com.globus.erpar.model.GmARCustPOModel;
/**
 * This GmPODrillDownRowMapper class used to map the Order  Details. 
 * 
 */

public class GmPODrillDownRowMapper implements RowMapper<GmARCustPOModel>{
	@Autowired
	GmCommonClass gmCommonClass;
	
	@Override
	public GmARCustPOModel mapRow(ResultSet resultSet, int i) throws SQLException {
		GmARCustPOModel gmARCustPOModel = new GmARCustPOModel();
		gmARCustPOModel.setStrRepAcc(GmCommonClass.parseNull(resultSet.getString("strRepAcc")));
		gmARCustPOModel.setStrOrderId(GmCommonClass.parseNull(resultSet.getString("strOrderId")));
		gmARCustPOModel.setStrOrdDT(GmCommonClass.parseNull(resultSet.getString("strOrdDT")));
		gmARCustPOModel.setStrTotal(GmCommonClass.parseNull(resultSet.getString("strTotal")));
		gmARCustPOModel.setStrOrdertype(GmCommonClass.parseNull(resultSet.getString("strOrdertype")));
		gmARCustPOModel.setStrShipDt(GmCommonClass.parseNull(resultSet.getString("strShipDt")));
		gmARCustPOModel.setStrShipTrack(GmCommonClass.parseNull(resultSet.getString("strShipTrack")));
		gmARCustPOModel.setStrUserName(GmCommonClass.parseNull(resultSet.getString("strUserName")));
		gmARCustPOModel.setStrCustPONumber(GmCommonClass.parseNull(resultSet.getString("strPONumber")));
		gmARCustPOModel.setStrInvoice(GmCommonClass.parseNull(resultSet.getString("strInvoice")));
		gmARCustPOModel.setStrParentOrdId(GmCommonClass.parseNull(resultSet.getString("strParentOrdId")));
		return gmARCustPOModel;
	}
}
