package com.globus.common.dao;

import java.net.URLDecoder;
import java.text.DateFormat;

import javax.persistence.StoredProcedureQuery;

import org.springframework.jdbc.core.JdbcTemplate;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureParameter;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.globus.common.util.GmCommonClass;

/**
 * This GmEntityManager class used to create the entity Manger object, and set
 * the context and timezone values - based on parameter values.
 *
 */
@Transactional
@Repository
public class GmEntityManager {

	@PersistenceContext
	public EntityManager em;

	public void getConnection(String strCompanyId, String strPlantId,
			String strTimeZone) {

		// to set the time zone.
		em.createNativeQuery("alter session set time_zone= '" + strTimeZone + "'")
				.executeUpdate();

		// context
		StoredProcedureQuery storedProcedureQuery1 = em
				.createStoredProcedureQuery("gm_pkg_cor_client_context.gm_sav_client_context")
				.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
				.setParameter(1, strCompanyId)
				.setParameter(2, strPlantId);
		storedProcedureQuery1.execute();

	}
}
