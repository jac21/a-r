package com.globus.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * This GmCommon class implements the null check and provide the property value
 * from the common properties
 * 
 * @author Sathish John
 * @since 2019-07-10
 */
@Configuration
@PropertySource(ignoreResourceNotFound = true, value = "classpath:application.properties")
@PropertySource(ignoreResourceNotFound = true, value = "classpath:error.properties")
@PropertySource(ignoreResourceNotFound = true, value = "classpath:lookup.properties")
@PropertySource(ignoreResourceNotFound = true, value = "classpath:message.properties")
public class GmCommonClass {

	@Autowired
	private Environment env;

	public static DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static DateFormat targetFormat = new SimpleDateFormat("MM/dd/yyyy");

	/**
	 * This method is used to check null and return empty values
	 * 
	 * @author Sathish John
	 * @since 2019-07-10
	 */
	public static String parseNull(Object object) {
		return (object == null || object.toString().equals("")) ? "" : object.toString().trim();
	}

	/**
	 * This method is used to check null and return Zero
	 *
	 * @author Sathish John
	 * @since 2019-07-10
	 */
	public static String parseZero(Object object) {
		return (object == null || object.toString().equals("")) ? "0" : object.toString().trim();
	}

	/**
	 * This method is used to get the value from common.properties and return the
	 * value
	 *
	 * @author Sathish John
	 * @since 2019-08-17
	 */
	public String getProperty(String pPropertyKey) {
		return env.getProperty(pPropertyKey);
	}
}
