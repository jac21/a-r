package com.globus.common.jms;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Configuration
@PropertySource(ignoreResourceNotFound = true, value = "classpath:application.properties")
public class GmJMSCommon {
	
	public static String providerUrl;
	public static String securityPrincipal;
	public static String securityCredetial;
	public static String initialContextFactory;
	public static String connectionFactory;
	public static String jmsClient;

    @Value("${providerUrl}")
    public void setProviderUrl(String providerurl) {
    	providerUrl = providerurl;
    }
    
    @Value("${securityPrincipal}")
    public void setSecurityPrincipal(String securityprincipal) {
    	securityPrincipal = securityprincipal;
    }
    
    @Value("${securityCredetial}")
    public void setSecurityCredetial(String securitycredetial) {
    	securityCredetial = securitycredetial;
    }
    
    @Value("${initialContextFactory}")
    public void setInitialContextFactory(String initialcontextfactory) {
    	initialContextFactory = initialcontextfactory;
    }
    
    @Value("${connectionFactory}")
    public void setConnectionFactory(String connectionfactory) {
    	connectionFactory = connectionfactory;
    }
    
	@Value("${jmsClient}")
    public void setJmsClient (String jmsClient) {
    	jmsClient = jmsClient;
    }
}
