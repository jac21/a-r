package com.globus.common.jms;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.stereotype.Component;
/**
 * GmQueueProducer - this class is used to call the jms
 *
 */
@Component
public class GmQueueProducer {

	private QueueConnectionFactory qconFactory;
	private QueueConnection qcon;
	private QueueSession qsession;
	private QueueSender qsender;
	private Queue queue;
	private ObjectMessage msg;
	private InitialContext ic;

	
	/**
	   * init - Will Initialize and start the connection to the queue to which the
	   * GmMessageTransferObject will be written asynchronously Use JMS_CLIENT_TO_USE switch to say
	   * which client to use for which the values are WEBLOGIC or JBOSS_LOCAL For remote connection to
	   * Jboss 7.1 , user name and password are required which are mapped to roles For same JVM
	   * connection user name /password will not be required In createQueueSession(false,
	   * Session.AUTO_ACKNOWLEDGE) false means that the session created to the Q is not transacted,
	   * AUTO_ACKNOWLEDGE means the message has been delivered to JMS server throws
	   * NamingException,JMSException
	   */
	private void init(Context ctx, String queueName) throws NamingException, JMSException {
		qconFactory = (QueueConnectionFactory) ctx.lookup(GmJMSCommon.connectionFactory);// lookup the queue connection
		/*if ((GmJMSCommon.jmsClient).equals("JBOSS_REMOTE")) {
			
			qcon = qconFactory.createQueueConnection(GmJMSCommon.securityPrincipal, GmJMSCommon.securityCredetial);
		}else{
			qcon = qconFactory.createQueueConnection();
		}*/
		
		qcon = qconFactory.createQueueConnection(GmJMSCommon.securityPrincipal, GmJMSCommon.securityCredetial);
		
		qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);// create a queue session
		queue = (Queue) ctx.lookup(queueName);// lookup the queue object
		qsender = qsession.createSender(queue);// create a queue sender
		msg = qsession.createObjectMessage();// create a Object message
		qcon.start();// start the connection
	}

	/**
	 * Send method which sends message to the queue
	 * 
	 * @throws JMSException
	 */
	private void send(Object message) throws JMSException {
		msg.setObject((Serializable) message);
		qsender.send(msg);
	}

	/**
	 * close method which closes all connection
	 * 
	 * @throws JMSException
	 *             NamingException
	 */
	private void close() throws JMSException, NamingException {

		if (qsender != null) {
			qsender.close();
		}
		if (qsession != null) {
			qsession.close();
		}
		if (qcon != null) {
			qcon.close();
		}
		/*
		 * Mihir Closing ic(Initial context object) will initialize the context again
		 * and will not throw Initial context exception when jboss JMS server is
		 * restarted and weblogic is not restarted. If weblogic and jboss are restarted
		 * the exception will never come.
		 */
		if (ic != null) {
			ic.close();

		}

	}

	/**
	 * sendMessage method which is used by user to send message. This method does
	 * the following jobs Initialize the context Initialize and start the connection
	 * to the Q Calls the private sendMsg method to send message Closes all open
	 * connection
	 * 
	 */
	public void sendMessage(HashMap hmParam, String strQueueNm) throws NamingException, JMSException, Exception {
		try {
			
			ic = getInitialContext();
			init(ic, strQueueNm);
			sendMsg(hmParam);
			close();
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		} finally {
			try {
				close();
			} catch (Exception e) {
				qsender = null;
				qsession = null;
				qcon = null;
				ic = null;

			}
		}
	}


	/**
	 * Delegate method sendMsg call send method to send message
	 * 
	 * @throws IOException
	 *             JMSException
	 */
	private void sendMsg(HashMap hmParam) throws IOException, JMSException {
		send(hmParam);
	}

	/**
	 * getInitialContext method initializes the context by setting factory class
	 * Fetches local host for Weblogic Set user name Password for remote Q
	 * 
	 * @throws IOException
	 *             JMSException
	 */
	public InitialContext getInitialContext() throws Exception {
		Hashtable<String, String> hashtable = new Hashtable<String, String>();
		
	/*	if ((GmJMSCommon.jmsClient).equals("JBOSS_REMOTE")) {

      hashtable.put(Context.INITIAL_CONTEXT_FACTORY, GmJMSCommon.initialContextFactory);
		hashtable.put(Context.SECURITY_PRINCIPAL, GmJMSCommon.securityPrincipal);
		hashtable.put(Context.SECURITY_CREDENTIALS, GmJMSCommon.securityCredetial);
      
    }*/
    //env.put(Context.PROVIDER_URL, MSGPROTOCOL + "://" + ipAddress + ":" + MSGPORT);
	
		
      hashtable.put(Context.INITIAL_CONTEXT_FACTORY, GmJMSCommon.initialContextFactory);
		hashtable.put(Context.SECURITY_PRINCIPAL, GmJMSCommon.securityPrincipal);
		hashtable.put(Context.SECURITY_CREDENTIALS, GmJMSCommon.securityCredetial);
		
		hashtable.put(Context.PROVIDER_URL, GmJMSCommon.providerUrl);
		return new InitialContext(hashtable);
	}
}