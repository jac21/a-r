package com.globus.common.model;

import java.util.Set;

public class GmKeycloakUser {
	private String username;
	private Set<String> roles;

	public Set<String> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}