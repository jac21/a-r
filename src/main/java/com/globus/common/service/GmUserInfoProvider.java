package com.globus.common.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.globus.common.dao.GmUserDao;
import com.globus.common.model.GmKeycloakUser;
import com.globus.common.model.GmUser;
import com.globus.common.util.GmCommonClass;

@Component
public class GmUserInfoProvider {

	@Autowired
	private GmKeycloakUtils gmKeycloakUtils;
	@Autowired
	private GmUserDao gmUserDao;
	@Autowired
	GmCommonClass gmCommonClass;
	@Autowired
	static GmUserInfoProvider gmUserInfoProvider;

	private Map<String, GmUser> userMap = new HashMap<>();

	public GmUser getUserInformation() throws Exception {
		GmKeycloakUser gmKeycloakUser = this.gmKeycloakUtils.getGmKeycloakUser();
		if (gmKeycloakUser.getRoles().contains(gmCommonClass.getProperty("tradeShowRole"))) {
			if (userMap.get(gmKeycloakUser.getUsername()) == null) {
				GmUser gmUser = this.gmUserDao.getUserInformation(gmKeycloakUser.getUsername());
				gmUser.setRoles(gmKeycloakUser.getRoles());

				this.userMap.put(gmKeycloakUser.getUsername(), gmUser);
			}
			return this.userMap.get(gmKeycloakUser.getUsername());
		} else {
			throw new Exception(gmCommonClass.getProperty("rolelogin"));
		}
	}

}
